// import Axios library to request the network
import axios from "axios"

// The function to create a new Axios instance
const instance = axios.create()

// Config the URL that will be requested
instance.defaults.baseURL = "https://jsonplaceholder.typicode.com/"

// Config header of the request
instance.defaults.headers = {
    "Content-Type": "application/json"
}

export default {
    onGetPosts(){
        // bring the instance that created to use GET method
        return instance.get("posts")
    },
    onCreatePost(body){
        // bring the instance that created to use POST method
        return instance.post("posts",body)
    },
    onUpdatePost(body){
        // bring the instance that created to use PUT method
        return instance.put("posts/"+body.id,body)
    },
    onDeletePost(id){
        // bring the instance that created to use DELETE method
        return instance.delete("posts/"+id)
    }
}