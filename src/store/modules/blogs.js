// import Service to request API 
import Service from "../../service/api";

const Blogs = {
  namespaced: true,
  state: {
    posts: [],
  },
  mutations: {
    onGetPosts(state, posts) {
      state.posts = posts;
    },
    onCreatePost(state,post){
      state.posts.unshift({
        id: post.id,
        userId: post.userId,
        body: post.body,
        title: post.title,
      });
    },
    onUpdatePost(state, postReturn){
      // (state.posts[index] = postReturn)
      state.posts.filter((post, index) => {
        post.id === postReturn.id ?  (
          state.posts.splice(index, 1, postReturn)
      ): null;
      });
    },
    onDeletePost(state, index){
      state.posts.splice(index, 1)
    }
  },
  actions: {
    onGetPosts({ commit }) {
      Service.onGetPosts()
        .then((res) => {
          commit("onGetPosts", res.data);
        })
        .catch((err) => console.log(err));
    },
    onCreatePost({commit}, body) {
      Service.onCreatePost(body)
        .then((res) => {
          commit("onCreatePost", res.data)
        })
        .catch((err) => console.log(err));
     },
    onUpdatePost({commit}, body) { 
      Service.onUpdatePost(body)
        .then((res) => {
          commit("onUpdatePost", res.data)
        })
        .catch((err) => console.log(err));
    },
    onDeletePost({commit}, body) { 
      Service.onDeletePost(body.id)
        .then(() => {
          commit("onDeletePost", body.index)
        })
        .catch((err) => console.log(err));
    },
  },
  getters: {
    posts(state) {
      return state.posts
    }
  },
};

export default Blogs;
